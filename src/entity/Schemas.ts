import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id?: number;
    @Column('text', { nullable: false })
    username!: string;
    @Column('text', { nullable: false })
    passwordHash!: string;
    /*
    @Column('text')
    sessionToken?: string; // This can be a jwt token with expiration
    */
}

@Entity()
export class Jam {
    @PrimaryGeneratedColumn()
    id!: number;
    @Column('text', { nullable: false })
    name!: string;
    @Column('text', { nullable: false })
    players!: string;
    @Column('text', { nullable: false })
    url!: string;
}