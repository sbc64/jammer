import { createConnection, ConnectionOptions, Connection, ConnectionManager, Timestamp } from "typeorm";
import { User } from "./entity/Schemas";
import { connect } from "http2";

const options: ConnectionOptions = {
    type: "postgres",
    host: "db",
    port: 5432,
    username: "test",
    password: "test",
    database: "jammer",
    synchronize: true,
    logging: true,
    // https://github.com/typeorm/typeorm/issues/2882#issuecomment-520269473
    entities: ['/app/build/entity/*.js']
}

let connection: Connection;
const initDb = async () => {
    let temp = await createConnection(options);
    if (temp.isConnected) {
        connection = temp;
    }
    let fillerUser = new User();
    fillerUser.username = "coolio";
    fillerUser.passwordHash = "hashyhashy";
    await connection.manager.save(fillerUser);
}

const createNewUser = (user: User): Promise<boolean> => {
    return connection.manager.save(user).then(result => {
        if (result.id) {
            return true;
        }
        return false;
    });
}

const getUser = async (username: string): Promise<User | void> => {
    let repository = connection.getRepository(User);
    return await repository.findOne({ username: username });
}

export default { initDb, createNewUser, getUser };