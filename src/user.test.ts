import { assert, expect } from 'chai';
import chai from 'chai';
import chaiHttp from 'chai-http';
import 'mocha';
chai.use(chaiHttp)

import app from "./index"
import user from './user';
import { User } from './entity/Schemas'

describe('Root Api live check', () => {
    it('should return response on root api call', async () => {
        let res = await chai.request(app).get('/');
        assert.equal(res.text, "hello");
    })
})

describe('User Api tests', () => {
    it('should return response on root api call', async () => {

        let res = await chai.request(app).post('/signup')
            .send({ username: "sebohe", passwordHash: "hash" });
        assert.equal(res.status, 200);
    })
})

describe("User Unit Test", () => {
    it('should return ok for new user  created', () => {
    })
})