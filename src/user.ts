import { Request } from 'express'
import { User } from './entity/Schemas'
import db from './db'

const exists = (username: string): Promise<boolean> => {
    return db.getUser(username).then(user => {
        if (user) {
            return true;
        }
        return false;
    });
}
const create = (user: User): Promise<boolean> => {
    return db.createNewUser(user);
}

const signup = (request: Request): boolean => {
    console.log(request)
    let newUser = new User();
    if (!request.body) {
        return false;
    }
    newUser.passwordHash = request.body.passwordHash;
    newUser.username = request.body.username;
    if (exists(newUser.username)) {
        return false
    }
    create(newUser).then();
    return true;
}

export default { signup, create, exists }