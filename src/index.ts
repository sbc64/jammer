import "reflect-metadata";
import express from 'express';

import db from './db';
import user from './user';

db.initDb();
const app = express();
const port = process.env.PORT || 3000;

app.post("/signup", (req, res) => {
    let status = 400;
    if (req.body && user.signup(req)) {
        status = 200;
    }
    res.status(status);
});

app.get("/", (_, res) => {
    res.status(200).send("hello");
});

app.listen(port, () => {
    console.log(`[SERVER] Running at http://localhost:3000`);
});

export default app