# Jammer :musical_note:

Please take a look at the [master branch](https://gitlab.com/sebohe/jammer/-/tree/master) for the latest updates.

## Dependencies:

- docker swarm
- nodejs >= 12

## Instructions

After having docker installed and runnign docker with your current user can run the project with:

```
npm i --dev
npm run dev:docker
```

You can then create a new user by running:

`bash ops/curl.sh`

I use [`nix-shell`](default.nix)instead of `npx` or `nvm` to handle my node environment. If you have either `nvm` or `npx` I recommend you use them for this project.

### Api Spec:

[Api Specification](api_specs.md)

The database Schemas defined in the spec drifted from the original specification when i realized that at the moment it wasn't feasible to implement. For example I would completely remove the Session table and just have a single session row for the users in database that has a jwt token with an expiration.

### Infrastructure diagram

![infra structure](infra.png)

### Why Docker-compose?

Because I'm really comfortable with it and it doesn't take much time for me to setup. Looking back at the time spent I could have definitely just ran a local postgres instance, or even just a a single docker container running postgres, but IMO having ssl certs is more important and mimicking production environment with the dev environment makes it much easier to integrate CI/CD pipelines.

### Separation between business logic and database logic

One of the design principles I told myself was to have `express.js` and `typeorm` not interfere with he business code to make future changes easier. Express.js would be contained in `index.ts` and all the database transactions would be contained within `db.ts`. This design choice makes it easy for upgrading

### Which api paths are working?

### Improvements in this current commit

I would remove the hard coded paths of the database and build paths of docker. For exmaple the mountpoint `/app` in the docker compose should be toggled with and environment variable.
