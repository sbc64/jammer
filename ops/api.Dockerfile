FROM node:12.13.0-alpine3.10
RUN apk add --update --no-cache bash curl g++ gcc git jq make python python3
RUN npm install -g nodemon