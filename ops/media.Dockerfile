FROM nginx:1.17-alpine
RUN ln -fs /dev/stdout /var/log/nginx/access.log && ln -fs /dev/stdout /var/log/nginx/error.log
COPY ./media.conf /etc/nginx/nginx.conf
ENTRYPOINT ["nginx"]
