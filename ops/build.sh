docker build -t local/api:latest -f ops/api.Dockerfile ./ops
docker build -t local/media:latest -f ops/media.Dockerfile ./ops
docker build -t local/proxy:latest -f ops/nginx/Dockerfile ./ops/nginx
