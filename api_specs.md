## Api paths

### Get

- `/jams`

Provides a list of all completed jams

User session authenticate: No

- `/jams/:user`

Provides a list of all the jams done by a certain user

User session authenticate: No

- `/jams/active`

Shows all jams that are currently being played

User session authenticate: Yes

- `/jams/pending`

Get all jams that are still looking for someone

User session authenticate: Yes

- `/jams/:jamId`

Listens to a jam that has already been played. Returns the media server/S3 bucket url.

User session authenticate: Yes

### Post

- `/jams/:jamId`

**Body:**
```
{
username,
instrument
}
```

Joins a jam session with the required instrument

User session authenticate: Yes

- `/signup`

**Body**

```
{
username,
passwordHash,
}
```

Creates a new user with the provided password hash

## Schemas:

Database schemas

#### UserShema
```
{
uid,
username,
passwordHash,
}

```

#### JamSchema
```
{
uid,
name,
players,
url,
}
```

#### Session Schema

```
{
uid,
username,
sessionToken,
expirationTime
}
```
